package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;

import java.util.Random;

import static java.lang.System.out;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Getter
@Setter
@AllArgsConstructor
public class Caballos implements Runnable {
    static int RANDOM_1 = new Random().nextInt(1, 3) * 1000;
    static int RANDOM_2 = new Random().nextInt(2, 5) * 1000;
    static int RANDOM_3 = new Random().nextInt(3, 7) * 1000;
    private String nombre;

    @SneakyThrows
    @Override
    public void run() {

        ProgressBar progressBar = new ProgressBar("Thread", 100);

        switch (nombre) {

            case "caballo1":
                int i = 0;
                progressBar.getStart();
                while (i < 100) {
                    Thread.sleep(RANDOM_1/100);
                    progressBar.step();
                    progressBar.setExtraMessage("caballo1 status...");

                    i++;
                }
                progressBar.close();
                break;

            case "caballo2":
                i = 0;
                progressBar.getStart();
                while (i < 100) {
                    Thread.sleep(RANDOM_2/100);
                    progressBar.step();
                    progressBar.setExtraMessage("caballo2 status...");

                    i++;
                }
                progressBar.close();
                break;

            case "caballo3":
                i = 0;
                progressBar.getStart();
                while (i < 100) {
                    Thread.sleep(RANDOM_3/100);
                    progressBar.step();
                    progressBar.setExtraMessage("caballo3 status...");

                    i++;
                }
                progressBar.close();

                break;

        }
    }
}
