package org.example;

import me.tongfei.progressbar.ProgressBar;

import javax.swing.*;
import java.util.*;

import static java.lang.System.in;
import static java.lang.System.out;
import static org.example.Caballos.*;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(in);

        out.println("Dame el nombre del caballo 1: ");
        String nombre_caballo_1 = scanner.next();

        out.println("Dame el nombre del caballo 2: ");
        String nombre_caballo_2 = scanner.next();

        out.println("Dame el nombre del caballo 3: ");
        String nombre_caballo_3 = scanner.next();

        Caballos caballo_1 = new Caballos(nombre_caballo_1);
        Thread thread_caballo1 = new Thread(caballo_1);

        out.printf("\nSale el caballo: %s", nombre_caballo_1);
        thread_caballo1.start();

        Caballos caballo_2 = new Caballos(nombre_caballo_2);
        Thread thread_caballo2 = new Thread(caballo_2);

        out.printf("\nSale el caballo: %s", nombre_caballo_2);
        thread_caballo2.start();

        Caballos caballo_3 = new Caballos(nombre_caballo_3);
        Thread thread_caballo3 = new Thread(caballo_3);

        out.printf("\nSale el caballo: %s", nombre_caballo_3);
        thread_caballo3.start();

        Map<Integer, String> map = new HashMap<>();
        map.put(RANDOM_1, nombre_caballo_1);
        map.put(RANDOM_2, nombre_caballo_2);
        map.put(RANDOM_3, nombre_caballo_3);

        List winnerList = Arrays.asList(RANDOM_1, RANDOM_2, RANDOM_3);
        Comparable winner = Collections.max(winnerList);
        out.printf("\n\n%s is the winner", map.get(winner));

    }
}